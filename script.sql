USE [master]
GO
/****** Object:  Database [TesteCRM ]    Script Date: 29/09/2019 16:06:37 ******/
CREATE DATABASE [TesteCRM ]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TesteCRM', FILENAME = N'C:\Users\acacio.oliveira\TesteCRM .mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'TesteCRM _log', FILENAME = N'C:\Users\acacio.oliveira\TesteCRM _log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [TesteCRM ] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TesteCRM ].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TesteCRM ] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TesteCRM ] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TesteCRM ] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TesteCRM ] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TesteCRM ] SET ARITHABORT OFF 
GO
ALTER DATABASE [TesteCRM ] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TesteCRM ] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TesteCRM ] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TesteCRM ] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TesteCRM ] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TesteCRM ] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TesteCRM ] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TesteCRM ] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TesteCRM ] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TesteCRM ] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TesteCRM ] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TesteCRM ] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TesteCRM ] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TesteCRM ] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TesteCRM ] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TesteCRM ] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TesteCRM ] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TesteCRM ] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TesteCRM ] SET  MULTI_USER 
GO
ALTER DATABASE [TesteCRM ] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TesteCRM ] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TesteCRM ] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TesteCRM ] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [TesteCRM ] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [TesteCRM ] SET QUERY_STORE = OFF
GO
USE [TesteCRM ]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [TesteCRM ]
GO
/****** Object:  Table [dbo].[TB_Usuario]    Script Date: 29/09/2019 16:06:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Usuario](
	[Email] [nvarchar](100) NOT NULL,
	[Nome] [nvarchar](200) NOT NULL,
	[Cpf] [nvarchar](11) NOT NULL,
	[DataNacimento] [datetime] NOT NULL,
	[Senha] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[Cpf] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [TesteCRM ] SET  READ_WRITE 
GO
