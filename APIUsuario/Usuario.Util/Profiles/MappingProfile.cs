﻿using AutoMapper;
using Usuario.Domain.Entities;
using Usuario.Repository.Abstractions.Model;

namespace Usuario.Util.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UsuarioModel, UsuarioEntity>().ReverseMap();
        }
    }
}
