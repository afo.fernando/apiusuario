﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Usuario.Repository.Abstractions.Model;

namespace Usuario.Repository.Mapping
{
    public class UsuarioMap : IEntityTypeConfiguration<UsuarioModel>
    {
        public void Configure(EntityTypeBuilder<UsuarioModel> builder)
        {
            builder.ToTable("TB_USUARIO").HasKey(x => x.Cpf);

            builder.Property(x => x.Email)
                .HasColumnName("Email")
                .HasColumnType("nvarchar(100)")
                .IsRequired();

            builder.Property(x => x.NomeCompleto)
                .HasColumnName("Nome")
                .HasColumnType("nvarchar(200)")
                .IsRequired();

            builder.Property(x => x.DataNascimento)
                .HasColumnName("DataNacimento")
                .HasColumnType("datetime")
                .IsRequired();

            builder.Property(x => x.senha)
             .HasColumnName("Senha")
             .HasColumnType("nvarchar(10)")
             .IsRequired();
        }
    }
}
