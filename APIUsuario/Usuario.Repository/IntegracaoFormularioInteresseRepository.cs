﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usuario.Repository.Abstractions;
using Usuario.Repository.Abstractions.Model;
using Usuario.Repository.Context;

namespace Usuario.Repository
{
    public class IntegracaoFormularioInteresseRepository : MySqlContext, IIntegracaoFormularioInteresseRepository
    {
        private readonly DbContextOptions<MySqlContext> _contextOptions;
        public IntegracaoFormularioInteresseRepository(DbContextOptions<MySqlContext> contextOptions) : base(contextOptions)
        {
            _contextOptions = contextOptions;
        }

        public async Task<IEnumerable<UsuarioModel>> ListarUsuarios()
        {
            var usuarios = await Usuarios.ToListAsync();
            return usuarios;
        }

        public async Task<UsuarioModel> ObterUsuario(string cpf)
        {
            var usuario = await Usuarios.FirstOrDefaultAsync(c => c.Cpf.Equals(cpf));
            return usuario;
        }

        public async Task SalvarDados(UsuarioModel usuario)
        {
            using (var context = new MySqlContext(_contextOptions))
            {
                context.Usuarios.Add(usuario);
                await context.SaveChangesAsync();
            }
        }
    }
}
