﻿using Microsoft.EntityFrameworkCore;
using Usuario.Repository.Abstractions.Model;
using Usuario.Repository.Mapping;

namespace Usuario.Repository.Context
{
    public class MySqlContext : DbContext
    {
        public MySqlContext(DbContextOptions<MySqlContext> contextOptions) : base(contextOptions)
        {

        }
        public DbSet<UsuarioModel> Usuarios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UsuarioModel>(new UsuarioMap().Configure);
        }

    }
}

