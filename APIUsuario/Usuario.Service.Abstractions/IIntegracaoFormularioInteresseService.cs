﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Usuario.Domain.Entities;

namespace Usuario.Service.Abstractions
{
    public interface IIntegracaoFormularioInteresseService
    {
        Task<(bool flag, string msg)> SalvarDados(UsuarioEntity usuario);
        Task<IEnumerable<UsuarioEntity>> ListarUsuarios();
        Task<UsuarioEntity> ObterUsuario(string cpf);

    }
}
