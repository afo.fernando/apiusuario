﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Usuario.Repository.Abstractions.Model
{
    public class UsuarioModel
    {
        public string NomeCompleto { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public DateTime DataNascimento { get; set; }
        public string senha { get; set; }
    }
}
