﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Usuario.Repository.Abstractions.Model;

namespace Usuario.Repository.Abstractions
{
    public interface IIntegracaoFormularioInteresseRepository
    {
        Task SalvarDados(UsuarioModel usuario);
        Task<IEnumerable<UsuarioModel>> ListarUsuarios();
        Task<UsuarioModel> ObterUsuario(string cpf);
    }
}
