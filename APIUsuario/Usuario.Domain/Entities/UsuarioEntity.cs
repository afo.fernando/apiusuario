﻿using Microsoft.AspNetCore.Mvc;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Web.Mvc;

namespace Usuario.Domain.Entities
{
    public class UsuarioEntity
    {
        [FromForm(Name = "fullname")]
        public string NomeCompleto { get; set; }

        [FromForm(Name = "cad_cpf")]
        public string Cpf { get; set; }

        [FromForm(Name = "emailaddress1")]
        public string Email { get; set; }

        [FromForm(Name = "cad_datanascimento")]
        public DateTime DataNascimento { get; set; }

        [FromForm(Name = "password")]
        public string senha { get; set; }
    }
}
