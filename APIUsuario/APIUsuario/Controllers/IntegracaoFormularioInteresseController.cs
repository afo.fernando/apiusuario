﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Usuario.Domain.Entities;
using Usuario.Service.Abstractions;

namespace APIUsuario.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IntegracaoFormularioInteresseController : ControllerBase
    {
        private readonly IIntegracaoFormularioInteresseService _integracaoFormularioInteresseService;

        public IntegracaoFormularioInteresseController(IIntegracaoFormularioInteresseService integracaoFormularioInteresseService)
        {
            _integracaoFormularioInteresseService = integracaoFormularioInteresseService;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UsuarioEntity>>> GetAsync()
        {
            var usuarios = await _integracaoFormularioInteresseService.ListarUsuarios();
            return Ok(usuarios);
        }

        // GET api/values/5
        [HttpGet("{cpf}")]
        public async Task<ActionResult<UsuarioEntity>> GetAsync(string cpf)
        {
            var usuario = await _integracaoFormularioInteresseService.ObterUsuario(cpf);
            if (usuario == null)
                return NotFound();

            return Ok(usuario);
        }

        // POST api/values
        [HttpPost("SalvarDados")]
        [Consumes("application/x-www-form-urlencoded")]
        public async Task<IActionResult> PostAsync([FromForm] UsuarioEntity value)
        {
            var (flag, msg) = await _integracaoFormularioInteresseService.SalvarDados(value);

            return !flag
             ? Ok(new { code = -1, data = new List<string>(), msg })
             : Ok(value);
        }

    }
}