﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Usuario.Repository;
using Usuario.Repository.Abstractions;
using Usuario.Service;
using Usuario.Service.Abstractions;

namespace APIUsuario
{
    public static class ServicesExtensions
    {
        public static void RegistrarRepositorios(this IServiceCollection services)
        {
            services.TryAddScoped<IIntegracaoFormularioInteresseRepository, IntegracaoFormularioInteresseRepository>();
        }

        public static void RegistrarServicos(this IServiceCollection services)
        {
            services.TryAddScoped<IIntegracaoFormularioInteresseService, IntegracaoFormularioInteresseService>();

        }
    }
}
