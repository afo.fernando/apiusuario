﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Usuario.Domain.Entities;

namespace Usuario.Service.Validators
{
    public class UsuarioValidator : AbstractValidator<UsuarioEntity>
    {
        public UsuarioValidator()
        {
            RuleFor(x => x.Email).EmailAddress().WithMessage("Email invalido");
            RuleFor(x => x.Cpf).NotEmpty().Must((user, cpf) => IsCpf(cpf)).WithMessage("CPF invalido");
            RuleFor(x => x.NomeCompleto).NotEmpty().WithMessage("Nome vazio");
            RuleFor(x => x.senha).Length(4, 10).WithMessage("Senha deve ter no mínimo 4 e no maximo 10 caracteres");
            RuleFor(x => x.senha).Must((user, senha) => IsSenha(senha)).WithMessage("A senha deve conter no mínimo 1 número, 1 caracter maiúsculo e 1 caracter especial");



        }

        private static bool IsCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            cpf = cpf.Trim().Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;

            for (int j = 0; j < 10; j++)
                if (j.ToString().PadLeft(11, char.Parse(j.ToString())) == cpf)
                    return false;

            string tempCpf = cpf.Substring(0, 9);
            int soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

            int resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            string digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            return cpf.EndsWith(digito);
        }

        private static bool IsSenha(string senha)
        {
            Regex regex = new Regex(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{4,}$");
            if (!regex.IsMatch(senha))
                return false;
            return true;
        }
    }
}
