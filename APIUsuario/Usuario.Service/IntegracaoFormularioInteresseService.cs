﻿using AutoMapper;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usuario.Domain.Entities;
using Usuario.Repository.Abstractions;
using Usuario.Repository.Abstractions.Model;
using Usuario.Service.Abstractions;

namespace Usuario.Service
{
    public class IntegracaoFormularioInteresseService : IIntegracaoFormularioInteresseService
    {
        private readonly IValidator<UsuarioEntity> _validator;
        private readonly IIntegracaoFormularioInteresseRepository _integracaoFormularioInteresseRepository;
        private readonly IMapper _mapper;

        public IntegracaoFormularioInteresseService(
            IValidator<UsuarioEntity> validator,
            IIntegracaoFormularioInteresseRepository integracaoFormularioInteresseRepository,
            IMapper mapper)
        {
            _validator = validator;
            _integracaoFormularioInteresseRepository = integracaoFormularioInteresseRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UsuarioEntity>> ListarUsuarios()
        {
            var usuarios = await _integracaoFormularioInteresseRepository.ListarUsuarios();
            return _mapper.Map<IEnumerable<UsuarioEntity>>(usuarios);
        }

        public async Task<UsuarioEntity> ObterUsuario(string cpf)
        {
            var usuario = await _integracaoFormularioInteresseRepository.ObterUsuario(cpf);
            return _mapper.Map<UsuarioEntity>(usuario);
        }

        public async Task<(bool flag, string msg)> SalvarDados(UsuarioEntity usuario)
        {
            var res = _validator.Validate(usuario);

            if (!res.IsValid)
                return (false, res.Errors.FirstOrDefault().ErrorMessage);

            var usuarioModel = _mapper.Map<UsuarioModel>(usuario);
            await _integracaoFormularioInteresseRepository.SalvarDados(usuarioModel);
            return (true, string.Empty);

        }
    }
}
